package se.syson.nessie.find;

import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ApplicationIntegrationTest {

    private static final int SCALE = 6;
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
    private static final int MAX_ITERATIONS = 1000;

    @Value("${local.server.port}")
    private int serverPort;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = serverPort;
    }

    @Test
    public void testFindNessieToBeTwoThirdsProbability() throws Exception {
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal count = BigDecimal.ZERO;
        BigDecimal average = BigDecimal.ZERO.setScale(SCALE, ROUNDING_MODE);
        final BigDecimal expected = getExpected();
        while(average.compareTo(expected) != 0 && total.intValue() < MAX_ITERATIONS) {
            if(isFound()){
                count = addOne(count);
            }
            total = addOne(total);
            average = divideBy(total, count);
        }
        assertTrue("Number of maximum iteration exceeded", total.intValue() < MAX_ITERATIONS);
        assertTrue("Expected probability not proven", average.compareTo(expected) == 0);
    }

    private BigDecimal divideBy(final BigDecimal total, final BigDecimal count) {
        return count.divide(total, SCALE, ROUNDING_MODE);
    }

    private BigDecimal addOne(final BigDecimal count) {
        return count.add(BigDecimal.ONE);
    }

    private BigDecimal getExpected() {
        return divideBy(new BigDecimal(3), new BigDecimal(2));
    }

    private boolean isFound() {
        return when()
                        .get("/")
                        .andReturn()
                        .as(Response.class)
                        .isFound();
    }

}