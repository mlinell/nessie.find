package se.syson.nessie.find;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FindNessieControllerTest {

    @InjectMocks
    private FindNessieController findNessieController;

    @Mock
    private FindNessieSimulator findNessieSimulator;

    @Mock
    private ResponseTranslator responseTranslator;

    @Test
    public void test(){
        final Response responseMock = new Response(true);
        when(findNessieSimulator.simulate()).thenReturn(true);
        when(responseTranslator.translate(eq(true))).thenReturn(responseMock);

        final Response response = findNessieController.simulate();

        assertSame(responseMock, response);
    }

}
