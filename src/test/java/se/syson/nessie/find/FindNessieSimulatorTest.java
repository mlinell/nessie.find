package se.syson.nessie.find;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FindNessieSimulatorTest {

    @InjectMocks
    private FindNessieSimulator findNessieSimulator;

    @Mock
    private RandomProducer randomProducer;

    @Test
    public void testShouldReturnFalseWhenRandomValuesIsEquals(){
        when(randomProducer.generateRandomInt(eq(3))).thenReturn(2, 2);

        final boolean found = findNessieSimulator.simulate();

        assertFalse(found);
    }

    @Test
    public void testShouldReturnTrueWhenRandomValuesIsNotEquals(){
        when(randomProducer.generateRandomInt(eq(3))).thenReturn(1, 2);

        final boolean found = findNessieSimulator.simulate();

        assertTrue(found);
    }

}
