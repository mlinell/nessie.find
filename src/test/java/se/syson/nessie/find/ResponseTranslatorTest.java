package se.syson.nessie.find;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ResponseTranslatorTest {

    @InjectMocks
    private ResponseTranslator responseTranslator;

    @Test
    public void testTranslator(){
        final Response response = responseTranslator.translate(true);

        assertTrue(response.isFound());
    }

}
