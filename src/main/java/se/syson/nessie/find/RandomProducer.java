package se.syson.nessie.find;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
class RandomProducer {

    int generateRandomInt(final int size){
        return new Random().nextInt(size);
    }

}
