package se.syson.nessie.find;

public final class Response {

    private boolean found;

    private Response(){}

    public Response(final boolean found){
      this.found = found;
    }

    public boolean isFound(){
      return found;
    }

}
