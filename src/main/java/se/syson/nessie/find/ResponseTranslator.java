package se.syson.nessie.find;

import org.springframework.stereotype.Component;

@Component
class ResponseTranslator {

    Response translate(final boolean found){
      return new Response(found);
    }

}
