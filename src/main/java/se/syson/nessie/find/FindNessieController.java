package se.syson.nessie.find;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
class FindNessieController {

    @Autowired
    private FindNessieSimulator findNessieSimulator;

    @Autowired
    private ResponseTranslator responseTranslator;

    @RequestMapping("/")
    @ResponseBody Response simulate() {
        return responseTranslator.translate(
                findNessieSimulator.simulate());
    }

}
