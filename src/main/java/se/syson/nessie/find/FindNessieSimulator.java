package se.syson.nessie.find;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
class FindNessieSimulator {

    private static final int NUMBER_OF_CHOICES = 3;

    @Autowired
    private RandomProducer randomProducer;

    /**
     *  Simulate the outcome of finding Nessie if you change your mind and go to the third lake instead of the first.
     *  When changing the mind, the given guess and the actual lake where nessie is located cannot be the same.
     * @return <code>true</code> if nessie is found or <code>false</code> otherwise
     */
    boolean simulate() {
        final int guess = randomProducer.generateRandomInt(NUMBER_OF_CHOICES);
        final int actual = randomProducer.generateRandomInt(NUMBER_OF_CHOICES);
        return actual != guess;
    }

}